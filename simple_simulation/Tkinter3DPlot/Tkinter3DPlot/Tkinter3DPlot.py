﻿import tkinter as tk
import math

class TkCanvas3D:
    def __init__(self, master, canvas_width, canvas_height, background="gray15"):
        self.canvas = tk.Canvas(master, width=canvas_width, height=canvas_height, bg=background)
        self.canvas.grid(row=0, column=0)
        self.canvas_width = canvas_width
        self.canvas_height = canvas_height
        self.canvas_diagonal = math.sqrt(canvas_height**2+canvas_height**2)
        self.setOrigin(0, 0, 0)
        self.setZaxis(45,1)
        self.unit = 1

    def setOrigin(self, x, y, z):
        self.origin = (x, y, z)

    def setUnit(self, new_unit):
        self.unit = new_unit

    def setZaxis(self, angle, distance):
        self.x_from_z = math.cos(angle/180.0*math.pi) * distance
        self.y_from_z = -math.sin(angle/180.0*math.pi) * distance
        return

    def get2Dfrom3D(self, x, y, z):
        x2D = x + self.x_from_z * z
        y2D = y + self.y_from_z * z
        return (x2D, y2D)

    def drawLine(self, from_point, to_point, color="white", tag="line", dash=False):
        from2D = self.get2Dfrom3D(self.origin[0] + from_point[0], self.origin[1] - from_point[1], self.origin[2] + from_point[2])
        to2D = self.get2Dfrom3D(self.origin[0] + to_point[0], self.origin[1] - to_point[1], self.origin[2] + to_point[2])
        if not dash:
            self.canvas.create_line(from2D[0], from2D[1], to2D[0], to2D[1], tags=tag, fill=color)
        else:
            self.canvas.create_line(from2D[0], from2D[1], to2D[0], to2D[1], tags=tag, fill=color, dash=dash)

    def drawGrid(self, grid_color="gray45"):
        origin2D = self.get2Dfrom3D(self.origin[0], self.origin[1], self.origin[2])
        zAxisPositive = canvas3d.get2Dfrom3D(self.origin[0], self.origin[1], self.origin[2] + self.canvas_diagonal)
        zAxisNegative = canvas3d.get2Dfrom3D(self.origin[0], self.origin[1], self.origin[2] - self.canvas_diagonal)

        self.canvas.create_line(0, origin2D[1], self.canvas_width, origin2D[1],
                                tags="origin", fill=grid_color, arrow=tk.LAST, arrowshape="16 20 6")
        self.canvas.create_line(origin2D[0], self.canvas_height, origin2D[0], 0,
                                tags="origin", fill=grid_color, arrow=tk.LAST, arrowshape="16 20 6")
        self.canvas.create_line(zAxisNegative[0], zAxisNegative[1], zAxisPositive[0], zAxisPositive[1],
                                tags="origin", fill=grid_color, arrow=tk.LAST, arrowshape="16 20 6")

    def addPointLocation(self, x, y, z, tag="3Dpoint", use_color=True):
        if use_color == True:
            color_x = "red"
            color_y = "dodger blue"
            color_z = "chartreuse"
        else:
            color_x = "gray75"
            color_y = "gray75"
            color_z = "gray75"

        self.get2Dfrom3D(self.origin[0], self.origin[1], self.origin[2])
        self.drawLine((0, 0, z), (x, 0, z), color_x, dash=(1,1), tag=tag)
        self.drawLine((x, 0, 0), (x, 0, z), color_z, dash=(1,1), tag=tag)
        self.drawLine((x, 0, z), (x, y, z), color_y, dash=(1,1), tag=tag)
        
        self.drawLine((0, 0, z), (0, y, z), color_y, dash=(1,1), tag=tag)
        self.drawLine((0, y, 0), (0, y, z), color_z, dash=(1,1), tag=tag)
        self.drawLine((0, y, z), (x, y, z), color_x, dash=(1,1), tag=tag)

        self.drawLine((0, y, 0), (x, y, 0), color_x, dash=(1,1), tag=tag)
        self.drawLine((x, 0, 0), (x, y, 0), color_y, dash=(1,1), tag=tag)
        self.drawLine((x, y, 0), (x, y, z), color_z, dash=(1,1), tag=tag)
        return

    def drawDot(self, x, y, z, radius, color="white", tag="dot"):
        radius /= 2.0
        position2D = self.get2Dfrom3D(self.origin[0] + x, self.origin[1] - y, self.origin[2] + z)
        self.canvas.create_oval(position2D[0] - radius, position2D[1] - radius,
                                position2D[0] + radius, position2D[1] + radius, fill=color, tag=tag)

    def drawText(self, x, y, z, text, color="white", tag="text"):
        position2D = self.get2Dfrom3D(self.origin[0] + x, self.origin[1] - y, self.origin[2] + z)
        self.canvas.create_text(position2D[0], position2D[1], text=text, fill=color, tag=tag)

    def loop(self):
        self.canvas.mainloop()

        
class Winch:
    def __init__(self, canvas, x, y, z):
        self.canvas = canvas
        self.x = x
        self.y = y
        self.z = z

    def show(self, enable_axis_location=False, use_colored_axis=False):
        if (enable_axis_location == True):
            self.canvas.addPointLocation(self.x, self.y, self.z, use_color=use_colored_axis)
        self.canvas.drawDot(self.x, self.y, self.z, 5, "white")

class Module:
    def __init__(self, canvas, winch1, winch2, winch3, hooks_distance_to_center=0):
        self.canvas = canvas
        self.winch1 = winch1
        self.winch2 = winch2
        self.winch3 = winch3
        self.hooks_distance = hooks_distance_to_center
        self.x = (winch1.x + winch2.x + winch3.x) / 3.0
        self.y = 0
        self.z = (winch1.z + winch2.z + winch3.z) / 3.0

        print((winch1.x + winch2.x + winch3.x) / 3.0)
        print((winch1.y + winch2.y + winch3.y) / 3.0)
        print((winch1.z + winch2.z + winch3.z) / 3.0)
        self.position = (0, 0, 0)
        self.clean_canvas = True
        self.is_showing_cables = False
        self.is_showing_cables_info = False

        self.hook1 = (math.cos(150/180*math.pi)*self.hooks_distance, 0, math.sin(150/180*math.pi)*self.hooks_distance)
        self.hook2 = (math.cos(150/180*math.pi)*self.hooks_distance, 0, math.sin(150/180*math.pi)*self.hooks_distance)
        self.hook3 = (math.cos(150/180*math.pi)*self.hooks_distance, 0, math.sin(150/180*math.pi)*self.hooks_distance)

    def setRelativePosition(self, x, y, z):
        self.position = (x, y, z)

    def show(self, enable_axis_location=True):
        if (self.clean_canvas == False):
            self.canvas.canvas.delete("module")
        absolute_x = self.x + self.position[0]
        absolute_y = self.y + self.position[1]
        absolute_z = self.z + self.position[2]
        if (enable_axis_location):
            self.canvas.addPointLocation(absolute_x, absolute_y, absolute_z, use_color=True, tag="module")
        self.canvas.drawDot(absolute_x, absolute_y, absolute_z, 10, "white", tag="module")
        str_relative_position = "(" + str(round(self.x + self.position[0])) + ", " + str(round(self.y + self.position[1])) + ", " + str(round(self.z + self.position[2])) + ")"
        self.canvas.drawText(absolute_x, absolute_y-20, absolute_z, str_relative_position, tag="module")
        self.canvas.drawLine((absolute_x, absolute_y, absolute_z), (self.winch1.x, self.winch1.y, self.winch1.z), color="white", tag="module")
        self.canvas.drawLine((absolute_x, absolute_y, absolute_z), (self.winch2.x, self.winch2.y, self.winch2.z), color="white", tag="module")
        self.canvas.drawLine((absolute_x, absolute_y, absolute_z), (self.winch3.x, self.winch3.y, self.winch3.z), color="white", tag="module")
        self.is_showing_cables = True

    def displayCablesLength(self):
        absolute_x = self.x + self.position[0]
        absolute_y = self.y + self.position[1]
        absolute_z = self.z + self.position[2]
        distance_winch1 = math.sqrt((absolute_x-self.winch1.x)**2 + (absolute_y-self.winch1.y)**2 + (absolute_z-self.winch1.z)**2)
        distance_winch2 = math.sqrt((absolute_x-self.winch2.x)**2 + (absolute_y-self.winch2.y)**2 + (absolute_z-self.winch2.z)**2)
        distance_winch3 = math.sqrt((absolute_x-self.winch3.x)**2 + (absolute_y-self.winch3.y)**2 + (absolute_z-self.winch3.z)**2)

        str_winch1 = "winch1 : " + str(round(distance_winch1)) + " px"
        str_winch2 = "winch2 : " + str(round(distance_winch2)) + " px"
        str_winch3 = "winch3 : " + str(round(distance_winch3)) + " px"

        self.canvas.drawText(self.winch1.x, self.winch1.y + 20, self.winch1.z, str_winch1, tag="info")
        self.canvas.drawText(self.winch2.x, self.winch2.y + 20, self.winch2.z, str_winch2, tag="info")
        self.canvas.drawText(self.winch3.x, self.winch3.y + 20, self.winch3.z, str_winch3, tag="info")
        self.is_showing_cables_info = True

    def connectWinches(self):
        position_winch1 = (self.winch1.x, self.winch1.y, self.winch1.z)
        position_winch2 = (self.winch2.x, self.winch2.y, self.winch2.z)
        position_winch3 = (self.winch3.x, self.winch3.y, self.winch3.z)

        ground_projection_winch1 = (self.winch1.x, 0, self.winch1.z)
        ground_projection_winch2 = (self.winch2.x, 0, self.winch2.z)
        ground_projection_winch3 = (self.winch3.x, 0, self.winch3.z)
        
        self.canvas.drawLine(position_winch1, position_winch2, dash=True)
        self.canvas.drawLine(position_winch1, position_winch3, dash=True)
        self.canvas.drawLine(position_winch2, position_winch3, dash=True)

        self.canvas.drawLine(ground_projection_winch1, ground_projection_winch2, dash=True)
        self.canvas.drawLine(ground_projection_winch1, ground_projection_winch3, dash=True)
        self.canvas.drawLine(ground_projection_winch2, ground_projection_winch3, dash=True)

        self.canvas.drawLine(position_winch1, ground_projection_winch1, dash=True)
        self.canvas.drawLine(position_winch2, ground_projection_winch2, dash=True)
        self.canvas.drawLine(position_winch3, ground_projection_winch3, dash=True)

    def hideAll(self):
        if (self.is_showing_cables):
            self.canvas.canvas.delete("module")
            self.is_showing_cables = False

        if (self.is_showing_cables_info):
            self.canvas.canvas.delete("info")
            self.is_showing_cables = False
       
    def moveForward(self, scale=1):
         self.setRelativePosition(self.position[0], self.position[1], self.position[2] + 1 * scale)

    def moveBackward(self, scale=1):
         self.setRelativePosition(self.position[0], self.position[1], self.position[2] - 1 * scale)

    def moveLeft(self, scale=1):
         self.setRelativePosition(self.position[0] - 1 * scale, self.position[1], self.position[2])

    def moveRight(self, scale=1):
         self.setRelativePosition(self.position[0] + 1 * scale, self.position[1], self.position[2])

    def moveUp(self, scale=1):
         self.setRelativePosition(self.position[0], self.position[1] + 1 * scale, self.position[2])

    def moveDown(self, scale=1):
         self.setRelativePosition(self.position[0], self.position[1] - 1 * scale, self.position[2])

# Start 3D canvas
canvas_height = 1400
canvas_width = 1000


master = tk.Tk()

canvas3d = TkCanvas3D(master, canvas_height, canvas_width)
canvas3d.setOrigin(canvas_height / 4, canvas_width / 4 * 3, 0)
canvas3d.setZaxis(30,1) # z isometric angle and scale
canvas3d.drawGrid()

distance_between_winches = 400
height_of_winches = 300
x_offset = 0
z_offset = 0

# Creer un triangle isocele avec les trois treuils a hauteur egale
winch1 = Winch(canvas3d, x_offset, height_of_winches, z_offset)
winch2 = Winch(canvas3d, x_offset + distance_between_winches, height_of_winches, z_offset)
winch3 = Winch(canvas3d, x_offset + math.cos(60/180.0*math.pi)*distance_between_winches, height_of_winches,
               z_offset + math.sin(60/180.0*math.pi)*distance_between_winches)

# Display winches 3D locations
use_colored_axis = False
winch1.show()
winch2.show()
winch3.show()



module = Module(canvas3d, winch1, winch2, winch3)
module.show()
module.displayCablesLength()
module.connectWinches()


def closeCanvas(event):
    master.destroy()

def moveUp(step, mod):
    mod.moveUp(step)
    mod.hideAll()
    mod.displayCablesLength()
    mod.show()

def moveDown(step, mod):
    mod.moveDown(step)
    mod.hideAll()
    mod.displayCablesLength()
    mod.show()

def moveLeft(step, mod):
    mod.moveLeft(step)
    mod.hideAll()
    mod.displayCablesLength()
    mod.show()

def moveRight(step, mod):
    mod.moveRight(step)
    mod.hideAll()
    mod.displayCablesLength()
    mod.show()

def moveForward(step, mod):
    mod.moveForward(step)
    mod.hideAll()
    mod.displayCablesLength()
    mod.show()

def moveBackward(step, mod):
    mod.moveBackward(step)
    mod.hideAll()
    mod.displayCablesLength()
    mod.show()

moving_step = 10

master.bind("<space>", closeCanvas)
master.bind("<Up>", lambda event, step=moving_step, mod=module: moveForward(step, mod))
master.bind("<Down>", lambda event, step=moving_step, mod=module: moveBackward(step, mod))
master.bind("<Left>", lambda event, step=moving_step, mod=module: moveLeft(step, mod))
master.bind("<Right>", lambda event, step=moving_step, mod=module: moveRight(step, mod))
master.bind("<Prior>", lambda event, step=moving_step, mod=module: moveUp(step, mod))
master.bind("<Next>", lambda event, step=moving_step, mod=module: moveDown(step, mod))

#t = tk.Toplevel()
#t = master
#t.wm_title("Other window")
#tk.Label(t, text="X").grid(row=0, column=1)
#tk.Label(t, text="Y").grid(row=1, column=1)
#tk.Label(t, text="Z").grid(row=2, column=1)
#e1 = tk.Entry(t)
#e2 = tk.Entry(t)
#e3 = tk.Entry(t)
#e1.grid(row=0, column=1)
#e2.grid(row=1, column=1)
#e3.grid(row=2, column=1)

#print(p1)
#print(p2)
#tkplot3D.canvas.create_line((p1[0], p1[1], p2[0], p2[1]), fill="white")
canvas3d.loop()