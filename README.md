# Wired drone

This is the temporary repository of the independant project named "*wire-suspended aerial vehicle remotely controlled*" or shorten "*wired drone*".


## Project introduction

The goal of the project is to conceive a system of a suspended plateform controlled by multiple remotely controlled winches, allowing the plateform to move freely and precisely in space.

More details are available in the documents "*Project Proposal*" and "*Project Report*" in the "*documentation*" section.